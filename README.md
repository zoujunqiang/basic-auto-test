[![star](https://gitee.com/by_cwc/basic-auto-test/badge/star.svg?theme=dark)](https://gitee.com/by_cwc/basic-auto-test)
[![fork](https://gitee.com/by_cwc/basic-auto-test/badge/fork.svg?theme=dark)](https://gitee.com/by_cwc/basic-auto-test)

# 自动化测试框架
- 使用技术：Python+Selenium3+Requests+Allure2+Pytest+Jenkins+Pymysql+openpyxl。
- 我们已经成功地在公司中落地了通用型框架！如果这个项目对你有帮助或启发，请多多点赞！
- 在 conftest.py 中，我们封装了公共的 driver 对象，实现了调用 driver 对象时自动识别本地 Chrome 版本并自动下载相应的 driver，从而免除了手动下载匹配的麻烦。此外，我们还对代码进行了适配，使其可以自动识别是否是 Linux 平台，从而使用无头浏览器和 Selenium Grid，为我们的测试工作提供了便利。
  - [![XRdTFH.jpg](https://s1.ax1x.com/2022/06/13/XRdTFH.jpg)](https://imgtu.com/i/XRdTFH)
##### 可以进行：
1. 接口自动化测试通过openpyxl进行数据驱动
2. UI自动化测试使用PageObjectModel模式进行
3. 集成jenkins后自动通过钉钉发送详细allure报告+简要内容报告。
   1. [![v25SN4.png](https://s1.ax1x.com/2022/08/26/v25SN4.png)](https://imgse.com/i/v25SN4)
4. 集成钉钉机器人可以针对场景进行钉钉通知。
#### 安装教程
1.  拉取项目安装requirements.txt中的包
2.  修改ConfigFile/config.yaml配置文件
3.  编写脚本执行
#### 备注
1. 不用自己安装chromedriver 会自动读取浏览器版本进行安装
#### 项目目录介绍
1. Commons --> 存放公共方法
   1. basic_request --> 请求接口 钉钉 dubbo（暂未实现）
   2. operation_file --> 操作yaml excel
   3. ui_auto --> ui自动化的basicPage 解析allure测试报告
   4. util --> 工具类 json解析 数据库操作 日志记录 等
   5. api_auto --> 接口自动化工具类
2. ConfigFile --> 项目的配置文件
3. PageObject --> po文件
4. TestCase --> 测试用例
5. TestData --> 测试数据
   1. TestCase.xlsx --> 不同的sheet存放不同的api文件
   2. SchemaData --> 存放校验接口字段的yaml文件
#### 更新日志
- 20220619
  1. 接口自动化支持多关键字校验按照下图在excel中维护用例（写一个参数就换行）
  2. Commons/api_auto/assert_method.py 支持接口多字段校验
     1. [![Xj28pV.jpg](https://s1.ax1x.com/2022/06/19/Xj28pV.jpg)](https://imgtu.com/i/Xj28pV)
- 20220907
  1. 新增ui自动化demo 
  2. 新增元素染色功能[![vHJmjS.jpg](https://s1.ax1x.com/2022/09/07/vHJmjS.jpg)](https://imgse.com/i/vHJmjS)


