# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : 通过jenkins发送allure测试报告


import os
import sys

import jenkins

from Commons.operation_file.operation_ymal import ReadYaml
from Commons.basic_request.ding_request import DingRobot


def send_report():
    conf = ReadYaml().get_every_config("Config")
    name = conf.get("TESTREPROT_OBJECT")
    job_url = conf.get('JOB_URL')
    jenkins_url = conf.get("JENKINS_URL")
    # 实例化jenkins对象
    jenkins_server = jenkins.Jenkins(
        url=jenkins_url,
        username=conf.get("JENKINS_ACCOUNT"),
        password=conf.get("JENKINS_PWD")
    )
    # 获取job最后一次的构建内容
    job_last_bulid = jenkins_server.get_info(job_url)["lastBuild"]["url"]
    # 测试报告地址
    report_url = job_last_bulid + "allure/"
    # 发送报告
    DingRobot(robot_name="oper_dingding_robot").res_allure_report(name=name, report_url=report_url)


if __name__ == '__main__':
    sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
    send_report()
