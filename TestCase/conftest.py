# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : 测试夹具

import platform

import pytest
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
from webdriver_manager.chrome import ChromeDriverManager

from Commons.operation_file.operation_ymal import ReadYaml

user = ReadYaml().get_every_config("Account")
conf = ReadYaml().get_every_config("Config")


def pytest_collection_modifyitems(items):
    """
    测试用例收集完成时，将收集到的item的name和nodeid的中文显示在控制台上
    :return:
    """
    for item in items:
        item.name = item.name.encode("utf-8").decode("unicode_escape")
        item._nodeid = item.nodeid.encode("utf-8").decode("unicode_escape")


# 代码提取
def get_drvier(model=None):
    # def get_drvier(model="debug"):
    global driver
    # 判断系统是否是linux如果是就返回true
    ishandless = True if platform.system() == 'Linux' else False
    # 服务器使用或者远程调试
    if model == "debug" or ishandless:
        option = webdriver.ChromeOptions()
        option.add_argument('--headless')
        option.add_argument('--no-sandbox')
        option.add_argument('--disable-gpu')
        option.add_argument('--disable-dev-shm-usage')
        option.add_argument('--hide-scrollbars')
        option.add_argument('--window-size=1920,1080')
        option.add_experimental_option('useAutomationExtension', False)
        option.add_experimental_option('excludeSwitches', ['enable-automation'])
        # command_executor 填写自己的grid地址
        driver = webdriver.Remote(command_executor='',
                                  desired_capabilities=DesiredCapabilities.CHROME, options=option)
        driver.implicitly_wait(conf.get("ALL_TIMEOUT"))
    # 本地调试使用
    else:
        path = ChromeDriverManager(cache_valid_range=7).install()
        driver = webdriver.Chrome(executable_path=path)
        driver.maximize_window()
    return driver


@pytest.fixture()
def global_step():
    _driver = get_drvier()
    yield _driver


@pytest.fixture()
def kill_driver():
    yield None
    driver.close()
    driver.quit()
    # os.system("ps aux | grep chromedriver | grep -v grep | awk '{print $2}' | xargs kill -9")
