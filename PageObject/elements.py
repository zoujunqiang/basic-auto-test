# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : 项目元素选择器
from selenium.webdriver.common.by import By as by


class PageBaidu:
    url = "https://www.baidu.com/"
    # 百度搜索内如输入框
    input = (by.XPATH, '//input[@id="kw"]')
    # baidu一下
    baiduButton = (by.XPATH, '//input[@id="su"]')
