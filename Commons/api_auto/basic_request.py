# -*- coding: utf-8 -*-
# @Author  : caiweichao
# @explain : 基本请求封装
from Commons.basic_request.http_request import HttpRequset
from Commons.util.temp_data import BasicData


def basicRequest(testCase: dict, data=None):
    if data:
        response = HttpRequset(method=testCase.get("method"), url=testCase.get("url"), data=data,
                               cookies=getattr(BasicData, "cookie"))
    else:
        response = HttpRequset(method=testCase.get("method"), url=testCase.get("url"), data=testCase.get("data"),
                               cookies=getattr(BasicData, "cookie"))
    return response